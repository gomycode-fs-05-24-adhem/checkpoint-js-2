//Renverser une chaine de caractère. En profiter pour reviser comment on déclare et appelle une fonction.
function strev(str) {
return str.split('').reverse('').join('')
}  
const revalue = strev("hello world, this is me")
console.log(revalue)


//Compter les éléments d'une chaine de caractère. Pour ne pas avoir d'erreur, je dois d'abord supprimer toèus les espaces dans le string en utilisant .split(' ') [l'espaces entre les quotes est capital]
function strlen(str){
return str.split(" ").join('').length
}
const lenvalue = strlen("count this")
console.log(lenvalue)

//Capitalise 1 : mettre toutes les lettres d'un mot en majuscule.
function strcaps(str){
    return str.toUpperCase()
}
const strupper = strcaps("mettre toutes les lettres d'un mot en majuscule.")
console.log(strupper)


//Capitalise 2 : mettre la première lettre des mots en majuscule. slice sélectione et coupe un mot à partir de l'index qui lui est donné en tant que valeur.
function strcaps2(str){
    console.log(str.slice(1))
    return str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
}
const strupper2 = strcaps2("mettre la première lettre des mots en majuscule.")
console.log(strupper2)


//mettre uniquement la première lettre du premier mot en majsucule.
function strcaps3(str){
    return str.charAt(0).toUpperCase() + str.slice(1)
}
const strupper3 = strcaps3("mettre uniquement la première lettre du premier mot en majsucule.")
console.log(strupper3)


//arr trouver le maximun au sein d'un tableau de nombres
function arrmax(arr){
    if (arr.length <= 0)
        {
            return "Votre tableau est vide"
        }
    let max = arr[0]
    for (let i = 1; i < arr.length; i++) {
        if (arr[i] > max)
            {
                max = arr[i]
            }
        return max
    }
}
let tabmax = arrmax([-54, 454, 564, 1, 5])
console.log("la valeur maximale du tableau est: " + tabmax)


//arr trouver le minimum au sein d'un tableau de nombres
function arrmin(arr)
{
    let min = Math.min(...arr)
    return min
    //je me suis fait chier parce que j'ai oublié de définir une fonction/variable à retourner.
}
let tabmin = arrmin([-55, 0, 15, 45454, 444, 488, -554])
console.log( "la valeur minimale du tableau est: " + tabmin)



//sum of array créer une fonction qui additionne tous les éléments d'un tableau avec while

function arrsum (arr)
{
    let sum = 0
    let i = 0
    while (i<arr.length) {
        sum = sum + arr[i]
        i = i+1
    }
    return sum
}
let tabsum = [0, -445, 51, 75, 16, 4496]
console.log("Le résultat de l'addition de arrsum est : " + arrsum(tabsum))

//sum of array créer une fonction qui soustrait tous les éléments d'un tableau avec arr.reduce
function arrsum2(arr)
{    
    return arr.reduce((a, b) => a-b)
}
let tabsum2 = [0, -445, 51, 75, 4496]
console.log(arrsum2(tabsum2))/*à revoir*/


//Filter Array: créer une fonction qui filtre certains éléments d'un tableau suivant une condition établie.
// T'as vu comment on utilise typeof : on met pas l'élément à qualifier entre parenthèses.
function filter(arr){
    return arr.filter(fil=> typeof fil === "number"
    )
}
let tabfiltre = [-55, 0, 15, 45454, 444, 488, -554, 'sdfFDsf','gdgdg']
console.log(filter(tabfiltre))


//Factorial: créer une fonction qui renvoie la valeur factorisée du nombre qu'elle reçoit.
function factor(vari)
{
    return  vari * 5
}
let factory  = 33 /*prompt("Entrez un nombre que vous souhaitez factoriser par 5 : ")*/
console.log (factor(factory))


//Prime Number Check: créer une fonction qui vérifie si un nombre est premier ou non
function primecheck (number)
{
    for (let i = 2; i < number; i++) {
        if (number%i !== 0)
            {
                return "ce nombre est premier"
            }
        else 
        {
            return "ce nombre n'est pas premier"
        }
    }
}
let numcheck = 33 /*prompt('Entrez le nombre que vous souhaitez checker : ')*/
console.log (primecheck(numcheck))


//Fibonacci Sequence: créer une suite de fibonacci
function fibonacci (fibo)
{
    let n1 = 0
    let n2 = 1
    let somme = 0
    let fiboarr = []
    fiboarr.push(n1)
    fiboarr.push(n2)
    for (let i = 2; i < fibo; i++) {
       somme = n1 + n2 
       n1 = n2 
       n2 = somme
       fiboarr.push(somme)     
    }
    return fiboarr 
}
console.log(("fiboarr : ") + fibonacci(10))